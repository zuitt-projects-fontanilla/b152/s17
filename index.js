//storing multiple values using variables
let student1 = "2020-01-17"
let student2 = "2020-01-20"
let student3 = "2020-1-25"

console.log(student1)
console.log(student2)
console.log(student3)

//storing multiple values in an array
const studentNumbers = ["2020-01-17", "2020-01-20", "202-01-25","202-02-25"]
console.log(studentNumbers)

//output the value of the element in the index 1 of the studentNumbers array
//all arrays start with index 0
//syntax: arrayName [index of value we want to output]
console.log(studentNumbers[1])
console.log("Index 3 of studentNumbers: ", studentNumbers[3]);
console.log( studentNumbers[4]) /* value is undefined*/

//arrays are declared using the square brackets aka Array literals
//each data stored insie an array is called an element
const emptyArray = [] //declares an empty array
const grades = [75, 85.5, 92,94]
const computerBrands = ["Acer", "Asus", "Lenovo", "Apple", "Redfox", "Gateway"]
console.log("Empty Array Sample:", emptyArray);
console.log(grades);
console.log("Computer Brands Array: ", computerBrands);

//all elements inside an array should have the same data types
//all elements inside an array should have the same with each other
const mixedArr = [12, "Asus", undefined, null, {}]

const tasks = [
	"drink html",
	"eat js",
	"inhale CSS",
	"bake sass"
]

console.log(tasks)


const fruits = ["apple", 'orange', 'kiwi', 'Dragon Fruit']
console.log("Value of fruits array before push()", fruits)

//push function adds an element at the end of an array
fruits.push('Mango')
console.log('Value of fruits array after push', fruits)

console.log('Value of fruits before pop()', fruits)
//removes the last element in the array
fruits.pop()
console.log('Value of fruits after pop()', fruits)

console.log('Value of fruits before unshift()', fruits)

//unshift adds one or more elements at the beginning of the array
fruits.unshift('strawberry')//adds strawberry to the beginning of the fruits array
console.log(fruits)
fruits.unshift('banana', 'raisins')
console.log(fruits)

//shift() removes the first element in our array

let removedFruit = fruits.shift()//expected output is banana. removed from array
console.log('You successfully removed the fruit:',removedFruit)
console.log(fruits)

//reverse reverses the order of elements in an array
fruits.reverse()
console.log(fruits)



/*tasks = [
	"drink html",
	"eat js",
	"inhale CSS",
	"bake sass"
]*/

console.log("Value of tasks before sort()", tasks)
tasks.sort()
console.log('Value of tasks after sort()', tasks)

const oddNumbers = [1, 3, 9, 5, 7]

//arranges the elements
console.log('Value of oddNumbers before sort()', oddNumbers)
oddNumbers.sort()
console.log('value of oddNumbers after sort(), oddNumbers')// 1,3,5,7,9

const countries = ['US', 'PH', 'CAN', 'SG', 'PH']
// indexOf() finds the index of a given element where it is FIrst found
let indexCountry = countries.indexOf('PH')
console.log('Index of PH', indexCountry)// 1
let lastIndexCountry = countries.lastIndexOf('PH')
console.log('Last Instance of PH: ', lastIndexCountry)// 4

console.log(countries)
//toString converts an array into a single value that is separated by a comma
console.log(countries.toString())


const subTasksA = ['drink html', 'eat JS']
const subTasksB = ['inhale css', 'breathe sass']
const subTasksC = ['get git', 'be node']


const subTasks = subTasksA.concat(subTasksB, subTasksC)
console.log(subTasks)

//join() converts an array into a single value, and is separated by specified character
console.log(subTasks.join('-'))
console.log(subTasks.join('@'))
console.log(subTasks.join('x'))


const users = ['blue', 'alexis', 'bianca', 'nikko', 'adrian']

console.log(users[0])//blue
console.log(users[1])//alexis

//syntax:

/*

	arrayName.forEach(function(eachElement)){
		console.log(eachElement)
	}



*/
//forEach iterates each element inside an array
//we pass each element as the parameter of the function declared insided the forEach()



//we have users array
//we want to iterate the users array using the forEach function
//each element is stired inside the variable called user
//logged in the browser the value of each user/each element inside the users array 

users.forEach(function(user){
	console.log("Name of Student: ", user)
})



users.forEach(function(user){

	if(user == 'blue'){
		console.log('Name fo Student: ', user)
	}
})




const numberList = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,51,99,101,102]

numberList.forEach(function(number){

	if (number % 2 == 0){
		console.log(number, " is an even number")
	}
	else {
		console.log (number, " is an odd number")
	}

})

//length property returns the number of elements inside an array.
console.log(numberList.length)// 18


//map() iterates each element and returns a new array depending on the result
//of the function's operation
//map() is useful when we will manipulate/change elements inside our array
//map() allows us to not touch/manipulate the original array


const numberData = numberList.map(function(number){
	return number * number 
})

console.log(numberData)
console.log(numberList)//not manipulated


//multidimensional arrays
//useful for storing complex data structures
const chessBoard = [
	['a1','b1','c1','d1','e1','f1','g1','h1'],
	['a2','b2','c2','d2','e2','f2','g2','h2'],
	['a3','b3','c3','d3','e3','f3','g3','h3'],
	['a4','b4','c4','d4','e4','f4','g4','h4'],
	
]	

console.log(chessBoard)// logs the whole chessBoard array
console.log(chessBoard[0])//logs the value in the index 0 of the chessboard
console.log(chessBoard[0][1]) // specific element inside the array in index 0


const students = ['aaron','bianca','masahiro']

function addStudent(name){

	students.push(name)
	//log the name of the student added and the new value of the student  array
	console.log(name + ' was added to the student list')
	console.log(students)
}

function countStudents(){
	//count the number of students inside the students array
	console.log(students.length)
	
}

function printStudents(){
	//sorts the array in alphanumeric order
	
	console.log(students)
	//logs each student namem in the browser (using forEach())
	students.forEach(function(student){
		console.log("Name of Student: ", student)
})


}


addStudent('Carlo')
addStudent('Adrian')
console.log(countStudents())
console.log(printStudents())






